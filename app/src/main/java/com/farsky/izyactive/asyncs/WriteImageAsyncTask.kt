package com.farsky.izyactive.asyncs

import android.content.Context
import android.os.AsyncTask
import com.farsky.izyactive.utils.FileSaver
import java.io.File

class WriteImageAsyncTask : AsyncTask<WriteImageAsyncTask.ImageAsyncTaskParam, Void, Boolean>() {

    override fun doInBackground(vararg params: ImageAsyncTaskParam) : Boolean {

        if (params.size == 0) return false

        val image = params[0]

        val dir = image.path.substring(0, image.path.lastIndexOf("/"))
        val fileName = image.path.substring(image.path.lastIndexOf("/") + 1)

        return FileSaver(image.context)
            .setFileName(fileName)
            .setDirectoryName(dir)
            .save(image.image)

    }

    data class ImageAsyncTaskParam(
        val image: File,
        val path: String,
        var context: Context
    )

}