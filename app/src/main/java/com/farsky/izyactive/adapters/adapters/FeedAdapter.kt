package com.farsky.izyactive.adapters.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.view_holders.FeedViewHolder
import com.farsky.izyactive.data.models.dto.FeedRVDto

class FeedAdapter(val context: Context) : RecyclerView.Adapter<FeedViewHolder>() {

    private val feeds: MutableList<FeedRVDto> = mutableListOf()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        val itemView = inflater.inflate(R.layout.view_holder_feed, parent, false)
        return FeedViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return feeds.size
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) {
        if (feeds.size > 0) {

            val current = feeds[position]

            holder.description.text = current.description

            val requestOptions: RequestOptions by lazy {
                RequestOptions()
                    .placeholder(R.drawable.ic_photo_black_24dp)
                    .override(500, 500)
                    .fitCenter()
                    .fallback(R.drawable.ic_photo_black_24dp)
                    .error(R.drawable.ic_photo_black_24dp)
            }
            Glide.with(context).load(current.photo)
                .apply(requestOptions)
                .into(holder.image)
        }
    }

    fun setFeeds(newFeeds: List<FeedRVDto>) {
        feeds.clear()
        feeds.addAll(newFeeds)
        notifyDataSetChanged()
    }

}