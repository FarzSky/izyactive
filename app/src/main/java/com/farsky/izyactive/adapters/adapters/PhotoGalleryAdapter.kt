package com.farsky.izyactive.adapters.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.view_holders.PhotoGalleryViewHolder
import com.farsky.izyactive.data.models.dto.PhotoGalleryDto


class PhotoGalleryAdapter(val context: Context) : RecyclerView.Adapter<PhotoGalleryViewHolder>() {

    private var photos: MutableList<PhotoGalleryDto> = mutableListOf()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoGalleryViewHolder {
        val itemView = inflater.inflate(R.layout.view_holder_photo_gallery, parent, false)
        return PhotoGalleryViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    override fun onBindViewHolder(holder: PhotoGalleryViewHolder, position: Int) {
        if (photos.size > 0) {
            val current = photos[position]
            val requestOptions: RequestOptions by lazy {
                RequestOptions()
                    .placeholder(R.drawable.ic_photo_black_24dp)
                    .override(500, 500)
                    .centerCrop()
                    .fallback(android.R.color.white)
                    .error(android.R.color.white)
            }
            Glide.with(context)
                .load(current.location)
                .apply(requestOptions)
                .into(holder.image)
        } else {
            Glide.with(context).load(android.R.color.white).into(holder.image)
        }
    }

    fun setPhotos(newPhotos: List<PhotoGalleryDto>) {
        photos.clear()
        photos = newPhotos.toMutableList()
        notifyDataSetChanged()
    }

    fun addPhoto(newPhoto: PhotoGalleryDto) {
        photos.add(newPhoto)
        notifyItemInserted(photos.size - 1)
    }

}