package com.farsky.izyactive.adapters.view_holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class ActivityBaseViewholder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(item: T)

}