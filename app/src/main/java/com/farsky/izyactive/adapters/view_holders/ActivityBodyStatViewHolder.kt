package com.farsky.izyactive.adapters.view_holders

import android.view.View
import com.farsky.izyactive.data.models.dto.ActivityBodyStatDto

class ActivityBodyStatViewHolder(itemView: View) : ActivityBaseViewholder<ActivityBodyStatDto>(itemView) {

    override fun bind(item: ActivityBodyStatDto) {
        //Do your view assignment here from the data model
    }
}