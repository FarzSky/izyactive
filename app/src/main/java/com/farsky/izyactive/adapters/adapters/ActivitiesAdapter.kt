package com.farsky.izyactive.adapters.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.view_holders.ActivityBaseViewholder
import com.farsky.izyactive.adapters.view_holders.ActivityBodyStatViewHolder
import com.farsky.izyactive.adapters.view_holders.ActivityMealViewHolder
import com.farsky.izyactive.adapters.view_holders.ActivityWorkoutViewHolder
import com.farsky.izyactive.data.models.dto.ActivityBodyStatDto
import com.farsky.izyactive.data.models.dto.ActivityMealDto
import com.farsky.izyactive.data.models.dto.ActivityWorkoutDto
import com.farsky.izyactive.utils.Constants

class ActivitiesAdapter(val context: Context) : RecyclerView.Adapter<ActivityBaseViewholder<*>>() {

    private val activities: MutableList<Any> = mutableListOf()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivityBaseViewholder<*> {
        return when(viewType) {
            Constants.ACTIVITY_WORKOUT -> {
                val itemView = inflater.inflate(R.layout.view_holder_activity_workout, parent, false)
                return ActivityWorkoutViewHolder(itemView)
            }
            Constants.ACTIVITY_MEAL -> {
                val itemView = inflater.inflate(R.layout.view_holder_activity_meal, parent, false)
                return ActivityMealViewHolder(itemView)
            }
            Constants.ACTIVITY_BODY_STAT -> {
                val itemView = inflater.inflate(R.layout.view_holder_activity_body_stat, parent, false)
                return ActivityBodyStatViewHolder(itemView)
            }
            else -> {
                throw IllegalArgumentException("Invalid view type")
            }
        }
    }

    override fun getItemCount(): Int {
        return activities.size
    }

    override fun onBindViewHolder(holder: ActivityBaseViewholder<*>, position: Int) {

        if (activities.size > 0) {

            val current = activities[position]

            when (holder) {
                is ActivityMealViewHolder -> holder.bind(current as ActivityMealDto)
                is ActivityWorkoutViewHolder -> holder.bind(current as ActivityWorkoutDto)
                is ActivityBodyStatViewHolder -> holder.bind(current as ActivityBodyStatDto)
                else -> throw IllegalArgumentException()
            }

        }
    }

    override fun getItemViewType(position: Int): Int {
        val comparable = activities[position]
        return when (comparable) {
            is ActivityMealDto -> Constants.ACTIVITY_MEAL
            is ActivityWorkoutDto -> Constants.ACTIVITY_WORKOUT
            is ActivityBodyStatDto -> Constants.ACTIVITY_BODY_STAT
            else -> throw IllegalArgumentException("Invalid type of data " + position)
        }
    }

    fun setActivities(newActivities: List<Any>) {
        activities.clear()
        activities.addAll(newActivities)
        notifyDataSetChanged()
    }

}