package com.farsky.izyactive.adapters.view_holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.farsky.izyactive.R

class WorkoutViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var img: ImageView = itemView.findViewById(R.id.workoutVHIV)
    var name: TextView = itemView.findViewById(R.id.workoutVHExerciseName)
    var time: TextView = itemView.findViewById(R.id.workoutVHTime)
    var duration: TextView = itemView.findViewById(R.id.workoutVHDuration)
    var calories: TextView = itemView.findViewById(R.id.workoutVHCalories)

}