package com.farsky.izyactive.adapters.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.view_holders.WorkoutViewHolder
import com.farsky.izyactive.data.models.dto.WorkoutDto

class WorkoutAdapter(val context: Context) : RecyclerView.Adapter<WorkoutViewHolder>() {

    private val workouts: MutableList<WorkoutDto> = mutableListOf()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkoutViewHolder {
        val itemView = inflater.inflate(R.layout.view_holder_workout, parent, false)
        return WorkoutViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return workouts.size
    }

    override fun onBindViewHolder(holder: WorkoutViewHolder, position: Int) {
        if (workouts.size > 0) {

            val current = workouts[position]

            holder.name.text = current.name
            holder.time.text = current.time
            holder.duration.text = current.duration.toString()
            holder.calories.text = current.calories.toString()

            val requestOptions: RequestOptions by lazy {
                RequestOptions()
                    .placeholder(R.drawable.ic_fitness_center_black_24dp)
                    .fitCenter()
                    .fallback(R.drawable.ic_fitness_center_black_24dp)
                    .error(R.drawable.ic_fitness_center_black_24dp)
            }
            Glide.with(context).load(current.imageLocation)
                .apply(requestOptions)
                .into(holder.img)
        }
    }

    fun setWorkouts(newWorkouts: List<WorkoutDto>) {
        workouts.clear()
        workouts.addAll(newWorkouts)
        notifyDataSetChanged()
    }

}