package com.farsky.izyactive.adapters.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.farsky.izyactive.utils.Constants.Companion.CODELIST_FEED_TYPE
import com.farsky.izyactive.views.photoGallery.PhotoGalleryPlaceholderFragment

class PhotoGallerySectionsAdapter(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return PhotoGalleryPlaceholderFragment.newInstance(position)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(CODELIST_FEED_TYPE[position]!!)
    }

    override fun getCount(): Int {
        return CODELIST_FEED_TYPE.size
    }

}