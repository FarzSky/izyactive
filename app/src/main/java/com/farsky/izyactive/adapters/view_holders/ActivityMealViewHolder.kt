package com.farsky.izyactive.adapters.view_holders

import android.view.View
import com.farsky.izyactive.data.models.dto.ActivityMealDto

class ActivityMealViewHolder(itemView: View) : ActivityBaseViewholder<ActivityMealDto>(itemView) {

    override fun bind(item: ActivityMealDto) {
        //Do your view assignment here from the data model
    }
}