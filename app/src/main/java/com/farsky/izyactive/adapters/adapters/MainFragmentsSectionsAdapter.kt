package com.farsky.izyactive.adapters.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.farsky.izyactive.utils.Constants.Companion.MAIN_FRAGMENT_TITLES
import com.farsky.izyactive.views.main.MainFragmentFactory

class MainFragmentsSectionsAdapter(private val context: Context, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return MainFragmentFactory.newInstance(position)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(MAIN_FRAGMENT_TITLES[position])
    }

    override fun getCount(): Int {
        return MAIN_FRAGMENT_TITLES.size
    }

}