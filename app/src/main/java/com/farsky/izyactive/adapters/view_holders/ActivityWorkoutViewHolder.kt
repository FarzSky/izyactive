package com.farsky.izyactive.adapters.view_holders

import android.view.View
import com.farsky.izyactive.data.models.dto.ActivityWorkoutDto

class ActivityWorkoutViewHolder(itemView: View) : ActivityBaseViewholder<ActivityWorkoutDto>(itemView) {

    override fun bind(item: ActivityWorkoutDto) {
        //Do your view assignment here from the data model
    }
}