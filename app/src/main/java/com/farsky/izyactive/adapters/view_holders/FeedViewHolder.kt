package com.farsky.izyactive.adapters.view_holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.farsky.izyactive.R

class FeedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var image: ImageView = itemView.findViewById(R.id.feedImageVH)
    var description: TextView = itemView.findViewById(R.id.feedDescriptionVH)

}