package com.farsky.izyactive.adapters.view_holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.farsky.izyactive.R

class MealViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var mealImg: ImageView = itemView.findViewById(R.id.foodVHIV)
    var mealType: TextView = itemView.findViewById(R.id.foodVHMealType)
    var mealTime: TextView = itemView.findViewById(R.id.foodVHMealTime)
    var mealCalories: TextView = itemView.findViewById(R.id.foodVHCalories)
    var mealName: TextView = itemView.findViewById(R.id.foodVHName)
    var mealGroceries: TextView = itemView.findViewById(R.id.foodVHGroceries)

}