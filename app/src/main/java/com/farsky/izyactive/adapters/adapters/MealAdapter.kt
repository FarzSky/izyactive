package com.farsky.izyactive.adapters.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.view_holders.MealViewHolder
import com.farsky.izyactive.data.models.dto.MealDto

class MealAdapter(val context: Context) : RecyclerView.Adapter<MealViewHolder>() {

    private val meals: MutableList<MealDto> = mutableListOf()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        val itemView = inflater.inflate(R.layout.view_holder_meal, parent, false)
        return MealViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return meals.size
    }

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) {
        if (meals.size > 0) {

            val current = meals[position]

            holder.mealType.text = current.timeOfDay
            holder.mealName.text = current.mealName
            holder.mealTime.text = current.mealTime
            holder.mealCalories.text = current.mealCalories.toString()
            holder.mealGroceries.text = current.mealGroceries?.joinToString(", ")

            val requestOptions: RequestOptions by lazy {
                RequestOptions()
                    .placeholder(R.drawable.infinity_spinner_light_green)
                    .fitCenter()
                    .fallback(R.drawable.ic_room_service_black_24dp)
                    .error(R.drawable.ic_room_service_black_24dp)
            }
            Glide.with(context).load(current.imageLocation)
                .apply(requestOptions)
                .into(holder.mealImg)
        }
    }

    fun setMeals(newMeals: List<MealDto>) {
        meals.clear()
        meals.addAll(newMeals)
        notifyDataSetChanged()
    }

}