package com.farsky.izyactive.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

class FileSaver(private val context: Context) {

    private var directoryName = "images"
    private var fileName = "image.png"
    private var directoryType = Environment.DIRECTORY_PICTURES
    private var external: Boolean = false

    fun setFileName(fileName: String): FileSaver {
        this.fileName = fileName
        val fileExt = fileName.split(".").last()
        directoryType = if (DIRECTORY_FOR_FILE.containsKey(fileExt)) DIRECTORY_FOR_FILE.get(fileExt) else Environment.DIRECTORY_DOWNLOADS
        return this
    }

    fun setExternal(external: Boolean): FileSaver {
        this.external = external
        return this
    }

    fun setDirectoryName(directoryName: String): FileSaver {
        this.directoryName = directoryName
        return this
    }

    fun save(bitmapImage: Bitmap): Boolean  {

        var result = false

        var fileOutputStream: FileOutputStream? = null
        try {
            fileOutputStream = FileOutputStream(createFile())
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
            result = true
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fileOutputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        return result
    }

    fun save(bytes: ByteArray): Boolean {

        var result = false

        var fileOutputStream: FileOutputStream? = null
        try {
            fileOutputStream = FileOutputStream(createFile())
            fileOutputStream.write(bytes)
            result = true
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fileOutputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        return result

    }

    fun save(file: File): Boolean {

        var result = false

        var fileOutputStream: FileOutputStream? = null
        try {
            fileOutputStream = FileOutputStream(createFile())
            fileOutputStream.write(file.readBytes())
            result = true
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fileOutputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        return result

    }

    public fun createFile(): File {

        val directory: File = if (external) {
            getExternalStorageDir(directoryName)
        } else {
            getInternalStorageDir(directoryName)
        }

        if (!directory.exists() && !directory.mkdirs()) {
            Log.e("FileSaver", "Error creating directory $directory")
        }

        return File(directory, fileName)
    }

    fun getFilePath(): String {

        var path: String = if (external) {
            getExternalStorageDir(directoryName).absolutePath
        } else {
            getInternalStorageDir(directoryName).absolutePath
        }

        return "$path/$fileName"

    }

    // Directory type is key for dir from Environment.DIRECTORY_*
    private fun getExternalStorageDir(directoryName: String): File {
        return File(
            context.getExternalFilesDir(directoryType), directoryName
        )
    }

    // Directory type is key for dir from Environment.DIRECTORY_*
    private fun getInternalStorageDir(directoryName: String): File {
        return File(
            context.getDir(directoryType, Context.MODE_PRIVATE), directoryName
        )
    }

    fun isExternalStorageWritable(): Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state
    }

    fun isExternalStorageReadable(): Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state || Environment.MEDIA_MOUNTED_READ_ONLY == state
    }

    fun load(): Bitmap? {
        var inputStream: FileInputStream? = null
        try {
            inputStream = FileInputStream(createFile())
            return BitmapFactory.decodeStream(inputStream)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                inputStream?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return null
    }

    companion object {
        val DIRECTORY_FOR_FILE = mapOf<String, String>(

            // images
            "jpg" to Environment.DIRECTORY_PICTURES,
            "jpeg" to Environment.DIRECTORY_PICTURES,
            "png" to Environment.DIRECTORY_PICTURES,
            "gif" to Environment.DIRECTORY_PICTURES,
            "tiff" to Environment.DIRECTORY_PICTURES,
            "webp" to Environment.DIRECTORY_PICTURES,

            // videos
            "webm" to Environment.DIRECTORY_MOVIES,
            "mkv" to Environment.DIRECTORY_MOVIES,
            "flv" to Environment.DIRECTORY_MOVIES,
            "avi" to Environment.DIRECTORY_MOVIES,
            "mp4" to Environment.DIRECTORY_MOVIES,
            "mpg" to Environment.DIRECTORY_MOVIES,
            "mpeg" to Environment.DIRECTORY_MOVIES,
            "3gp" to Environment.DIRECTORY_MOVIES,

            // music
            "mp3" to Environment.DIRECTORY_MUSIC,

            // documents
            "doc" to Environment.DIRECTORY_DOWNLOADS,
            "docx" to Environment.DIRECTORY_DOWNLOADS,
            "ppt" to Environment.DIRECTORY_DOWNLOADS,
            "pptx" to Environment.DIRECTORY_DOWNLOADS,
            "txt" to Environment.DIRECTORY_DOWNLOADS,
            "json" to Environment.DIRECTORY_DOWNLOADS,
            "html" to Environment.DIRECTORY_DOWNLOADS,
            "xml" to Environment.DIRECTORY_DOWNLOADS,
            "odf" to Environment.DIRECTORY_DOWNLOADS,
            "odt" to Environment.DIRECTORY_DOWNLOADS,
            "ods" to Environment.DIRECTORY_DOWNLOADS,
            "xls" to Environment.DIRECTORY_DOWNLOADS,
            "xlsx" to Environment.DIRECTORY_DOWNLOADS,
            "pdf" to Environment.DIRECTORY_DOWNLOADS,
            "zip" to Environment.DIRECTORY_DOWNLOADS,
            "7z" to Environment.DIRECTORY_DOWNLOADS,
            "rar" to Environment.DIRECTORY_DOWNLOADS

        )
    }
}