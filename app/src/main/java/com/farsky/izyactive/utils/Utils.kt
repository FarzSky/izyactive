package com.farsky.izyactive.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    companion object {

        @SuppressLint("SimpleDateFormat")
        fun dateTimeToString(datetime: Date): String {
            val pattern = "yyyyMMdd_HHmmss"
            val simpleDateFormat = SimpleDateFormat(pattern)
            return simpleDateFormat.format(Date())
        }

        @SuppressLint("SimpleDateFormat")
        fun dateTimeToEUString(datetime: Date): String {
            val pattern = "dd-MM-yyyy HH:mm"
            val simpleDateFormat = SimpleDateFormat(pattern)
            return simpleDateFormat.format(Date())
        }

    }

}