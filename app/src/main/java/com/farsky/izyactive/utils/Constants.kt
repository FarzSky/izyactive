package com.farsky.izyactive.utils

import com.farsky.izyactive.R

class Constants {

    companion object {

        const val CODELIST_MAIN_FRAGMENT_CAMERA = 0
        const val CODELIST_MAIN_FRAGMENT_HOME = 1
        val MAIN_FRAGMENT_TITLES = arrayOf(
            R.string.main_fragment_title_camera,
            R.string.main_fragment_title_home
        )

        const val CODELIST_FEED_TYPE_PROFILE = 0
        const val CODELIST_FEED_TYPE_WORKOUT = 1
        const val CODELIST_FEED_TYPE_MEAL = 2
        val CODELIST_FEED_TYPE = mapOf(
            CODELIST_FEED_TYPE_PROFILE to R.string.photo_gallery_tab_title_profile,
            CODELIST_FEED_TYPE_WORKOUT to  R.string.photo_gallery_tab_title_workout,
            CODELIST_FEED_TYPE_MEAL to  R.string.photo_gallery_tab_title_food
        )

        val CAMERA_SAVED_PHOTO_DATA_INTENT = "camera_saved_photo_data_intent"
        val CAMERA_SAVED_PHOTO_PATH_INTENT = "camera_saved_photo_name_intent"

        val REQUEST_CODE_CANCEL = 0
        val REQUEST_CODE_SUCCESS = 1

        const val ACTIVITY_WORKOUT = 0
        const val ACTIVITY_MEAL = 1
        const val ACTIVITY_BODY_STAT = 2

    }

}
