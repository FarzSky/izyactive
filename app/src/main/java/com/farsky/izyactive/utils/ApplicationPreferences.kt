package com.farsky.izyactive.utils

import android.content.Context
import android.content.SharedPreferences

class ApplicationPreferences private constructor(context: Context) {

    private val preferences: SharedPreferences
    private val storageHelper: StorageHelper

    companion object {

        private val APP_PACKAGE = "com.farsky.izyactive"

        val PREFERENCES_FILE = APP_PACKAGE + "_settings"

        private val CURRENTLY_LOGGED_USERNAME_KEY = APP_PACKAGE + "_currently_logged_username"
        // usernames and passwords for multiple ever logged users
        // format: username::username::username
        private val USERNAMES_KEY = APP_PACKAGE + "_usernames"
        private val PASSWORD_KEY = APP_PACKAGE + "_password_"

        private var instance: ApplicationPreferences? = null

        fun getInstance(context: Context): ApplicationPreferences {
            if (instance == null) {
                instance = ApplicationPreferences(context)
            }

            return instance as ApplicationPreferences
        }

    }

    val isUserLoggedIn: Boolean
        get() {
            val username = preferences.getString(CURRENTLY_LOGGED_USERNAME_KEY, null)
            return username != null
        }

    var currentUsername: String?
        get() = preferences.getString(CURRENTLY_LOGGED_USERNAME_KEY, null)
        set(username) {
            val editor = preferences.edit()
            editor.putString(CURRENTLY_LOGGED_USERNAME_KEY, username)
            editor.apply()
        }

    init {
        preferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE)
        storageHelper = StorageHelper(context)
    }

    fun getAllUsernames(): Collection<String> {
        val usernames = preferences.getString(USERNAMES_KEY, null)
        if (usernames == null) return emptyList()
        else  return usernames.split("::")
    }

    fun saveUser(username: String, password: String) {
        val usernames = getAllUsernames()
        if (!usernames.contains(username)) {
            val newUsernames = usernames.toMutableList()
            newUsernames.add(username)
            val editor = preferences.edit()
            editor.putString(USERNAMES_KEY, usernames.joinToString("::"))
            editor.apply()
        }

        storageHelper.setData(PASSWORD_KEY + username, password.toByteArray())

    }

    fun clearUsername(username: String) {
        val usernames = getAllUsernames()
        if (usernames.contains(username)) {
            val newUsernames = usernames.toMutableList()
            newUsernames.remove(username)
            val editor = preferences.edit()
            editor.putString(USERNAMES_KEY, newUsernames.joinToString("::"))
            editor.apply()
        }
        storageHelper.remove(PASSWORD_KEY + username)
    }

}