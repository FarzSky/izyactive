package com.farsky.izyactive.views.main

import androidx.fragment.app.Fragment
import com.farsky.izyactive.utils.Constants
import com.farsky.izyactive.views.camera.CameraFragment
import com.farsky.izyactive.views.home.HomeFragment

class MainFragmentFactory {

    companion object {

        @JvmStatic
        fun newInstance(sectionNumber: Int): Fragment {

            if (sectionNumber == Constants.CODELIST_MAIN_FRAGMENT_CAMERA) {
                return CameraFragment.newInstance(sectionNumber)
            } else if (sectionNumber == Constants.CODELIST_MAIN_FRAGMENT_HOME) {
                return HomeFragment.newInstance(sectionNumber)
            } else {
                return HomeFragment.newInstance(sectionNumber)
            }

        }
    }

}
