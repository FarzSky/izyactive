package com.farsky.izyactive.views.photoGallery

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.farsky.izyactive.data.models.dto.PhotoGalleryDto
import com.farsky.izyactive.data.repositories.FeedRepository
import com.farsky.izyactive.utils.FileSaver

class PhotoGalleryViewModel(application: Application) : AndroidViewModel(application) {

    private val _index = MutableLiveData(0)

    var feedImages: LiveData<List<PhotoGalleryDto>> = MutableLiveData(emptyList())

    private var feedRepository: FeedRepository = FeedRepository(application)

    private val fileSaver = FileSaver(application)

    fun setIndex(index: Int) {
        _index.value = index

        feedImages = Transformations.map(
            feedRepository.getImages(_index.value!!.toLong())
        )
        { input -> input.map { image ->
                var imageLocationTemp = image.path ?: ""
                if (imageLocationTemp != "") {
                    val dir = imageLocationTemp.substring(0, imageLocationTemp.lastIndexOf("/"))
                    val fileName = imageLocationTemp.substring(imageLocationTemp.lastIndexOf("/") + 1)
                    imageLocationTemp = fileSaver.setDirectoryName(dir).setFileName(fileName).getFilePath()
                }
                PhotoGalleryDto(location = imageLocationTemp)
            }
        }
    }

}