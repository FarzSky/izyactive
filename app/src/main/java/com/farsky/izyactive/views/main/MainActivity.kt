package com.farsky.izyactive.views.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.adapters.MainFragmentsSectionsAdapter
import com.farsky.izyactive.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainFragmentsSectionAdapter: MainFragmentsSectionsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainFragmentsSectionAdapter =
            MainFragmentsSectionsAdapter(
                this,
                supportFragmentManager
            )
        mainVP.adapter = mainFragmentsSectionAdapter
        mainVP.currentItem = Constants.CODELIST_MAIN_FRAGMENT_HOME
    }

}
