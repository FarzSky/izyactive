package com.farsky.izyactive.views.camera

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.farsky.izyactive.data.models.dto.FeedDto
import com.farsky.izyactive.data.repositories.FeedRepository
import java.io.File

class NewFeedViewModel(application: Application) : AndroidViewModel(application) {

    private var feedRepository: FeedRepository = FeedRepository(application)

    private var _feedDto = MutableLiveData<FeedDto>(FeedDto(null))
    var feedDto: LiveData<FeedDto> = _feedDto

    fun setPhoto(photo: File) {
        _feedDto.value?.photo = photo
    }

    fun setDescription(description: String) {
        _feedDto.value?.description = description
    }

    fun setFeedType(position: Int) {
        _feedDto.value?.feedType = position
    }

    fun publish() {
        feedDto.value!!.photo?.let {
            feedRepository.createFeed(feedDto.value!!.feedType.toLong(),
                feedDto.value!!.description,
                it
            )
        }
    }

}