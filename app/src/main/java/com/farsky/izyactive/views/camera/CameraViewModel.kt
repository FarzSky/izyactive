package com.farsky.izyactive.views.camera

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.farsky.izyactive.data.enums.CameraFacingEnum
import com.farsky.izyactive.data.enums.CameraFlashlightEnum
import java.io.File

class CameraViewModel : ViewModel() {

    private val _cameraFlashlightState = MutableLiveData<CameraFlashlightEnum>(CameraFlashlightEnum.OFF)
    val cameraFlashlightState: LiveData<CameraFlashlightEnum> = _cameraFlashlightState

    private val _cameraFacingState = MutableLiveData<CameraFacingEnum>(CameraFacingEnum.BACK)
    val cameraFacingState: LiveData<CameraFacingEnum> = _cameraFacingState

    var cameraSavedPhoto: File? = null

    fun toggleFlashlight() {
        when (cameraFlashlightState.value) {
            CameraFlashlightEnum.ON -> _cameraFlashlightState.value = CameraFlashlightEnum.OFF
            CameraFlashlightEnum.OFF -> _cameraFlashlightState.value = CameraFlashlightEnum.ON
            else -> _cameraFlashlightState.value = CameraFlashlightEnum.OFF
        }
    }

    fun toggleCameraFacing() {
        when (cameraFacingState.value) {
            CameraFacingEnum.BACK -> _cameraFacingState.value = CameraFacingEnum.FRONT
            CameraFacingEnum.FRONT -> _cameraFacingState.value = CameraFacingEnum.BACK
            else -> _cameraFacingState.value = CameraFacingEnum.BACK
        }
    }

    fun savePhoto(photo: File) {
        cameraSavedPhoto = photo
    }

    fun getSavedPhoto(): File? {
        return cameraSavedPhoto
    }

}