package com.farsky.izyactive.views.camera

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import android.widget.ToggleButton
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.farsky.izyactive.R
import com.farsky.izyactive.utils.Constants
import kotlinx.android.synthetic.main.activity_new_feed.*
import java.io.File

class NewFeedActivity : AppCompatActivity() {

    private lateinit var newFeedViewModel: NewFeedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_feed)

        newFeedViewModel = ViewModelProviders.of(this).get(NewFeedViewModel::class.java).apply {

            val path = intent.getStringExtra(Constants.CAMERA_SAVED_PHOTO_PATH_INTENT)

            if (path != null) {
                val file = File(path)
                file.writeBytes(file.readBytes())
                setPhoto(file)
            }

        }

        setPublishListener()
        setCancelListener()
        setFeedTypeListener()

        newFeedViewModel.feedDto.observe(this, Observer {
            val requestOptions: RequestOptions by lazy {
                RequestOptions()
                    .placeholder(R.drawable.ic_photo_black_24dp)
                    .override(500, 500)
                    .fitCenter()
                    .fallback(R.drawable.infinity_spinner_light_green)
                    .error(R.drawable.infinity_spinner_light_green)
            }
            Glide.with(this)
                .load(it.photo?.readBytes())
                .apply(requestOptions)
                .into(feedImagePreview)
        })

    }

    private fun setPublishListener() {
        feedPublish.setOnClickListener {
            newFeedViewModel.setDescription(feedDescription.text.toString())
            newFeedViewModel.publish()
            val intent = Intent()
            setResult(Constants.REQUEST_CODE_SUCCESS, intent)
            finish()
        }
    }

    private fun setCancelListener() {
        feedCancelPublish.setOnClickListener {
            val intent = Intent()
            setResult(Constants.REQUEST_CODE_CANCEL, intent)
            finish()
        }
    }

    private fun setFeedTypeListener() {
        feedTypeRadioGroup.setOnCheckedChangeListener { radioGroup, i ->
            for (j in 0 until radioGroup.childCount) {
                val view = radioGroup.getChildAt(j) as ToggleButton
                val isChecked = view.id == i
                view.isChecked = isChecked
                if (isChecked) newFeedViewModel.setFeedType(j)
            }
        }
    }

    fun onToggle(view: View) {
        (view.getParent() as RadioGroup).check(view.id)
    }

}