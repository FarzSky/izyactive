package com.farsky.izyactive.views.camera

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Size
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.farsky.izyactive.R
import com.farsky.izyactive.utils.Constants
import com.farsky.izyactive.utils.FileSaver
import kotlinx.android.synthetic.main.fragment_camera.*
import java.io.File
import java.util.concurrent.Executor

class CameraFragment : Fragment(), Executor {

    private lateinit var cameraViewModel: CameraViewModel
    private var imageCapture: ImageCapture? = null

    companion object {

        private const val ARG_SECTION_NUMBER = "main_section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): CameraFragment {
            return CameraFragment()
                .apply {
                    arguments = Bundle().apply {
                        putInt(ARG_SECTION_NUMBER, sectionNumber)
                    }
                }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cameraViewModel = ViewModelProviders.of(this).get(CameraViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_camera, container, false)

        view.findViewById<TextureView>(R.id.texture).post {
            bindCamera()
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setCameraFlashlightListener()
        setCameraFacingListener()
        setCameraPublishPhotoListener()
        setCameraShotListener()

    }

    private fun bindCamera() {

        if (hasNoPermissions()) requestPermission()

        CameraX.unbindAll()

        val metrics = DisplayMetrics().also { texture.display.getRealMetrics(it) }
        val screenSize = Size(metrics.widthPixels, metrics.heightPixels)

        val previewConfig = PreviewConfig.Builder()
            .setLensFacing(cameraViewModel.cameraFacingState.value!!.cameraFacingState)
            .setTargetResolution(screenSize)
            .setTargetRotation(activity?.windowManager?.defaultDisplay?.rotation ?: 0)
            .build()

        val preview = Preview(previewConfig)
        preview.setOnPreviewOutputUpdateListener {
            texture.surfaceTexture = it.surfaceTexture
        }

        // Create configuration object for the image capture use case
        val imageCaptureConfig = ImageCaptureConfig.Builder()
            .setLensFacing(cameraViewModel.cameraFacingState.value!!.cameraFacingState)
            .setFlashMode(cameraViewModel.cameraFlashlightState.value!!.flashlightState)
            .setTargetResolution(screenSize)
            .setTargetRotation(activity?.windowManager?.defaultDisplay?.rotation ?: 0)
            .setCaptureMode(ImageCapture.CaptureMode.MAX_QUALITY)
            .build()

        // Build the image capture use case and attach button click listener
        imageCapture = ImageCapture(imageCaptureConfig)

        CameraX.bindToLifecycle(this, imageCapture, preview)

    }

    private fun setCameraFlashlightListener() {
        cameraViewModel.cameraFlashlightState.observe(this@CameraFragment, Observer {
            cameraFlash.setImageResource(it.flashDrawable)
            imageCapture?.flashMode = it.flashlightState
        })

        cameraFlash.setOnClickListener {
            cameraViewModel.toggleFlashlight()
        }
    }

    private fun setCameraFacingListener() {
        cameraViewModel.cameraFacingState.observe(this@CameraFragment, Observer {
            cameraFacing.setImageResource(it.facingDrawable)
            bindCamera()
        })

        cameraFacing.setOnClickListener {
            cameraViewModel.toggleCameraFacing()
        }
    }

    private fun setCameraShotListener() {
        cameraShot.setOnClickListener {

            val file = FileSaver(requireContext()).setFileName("temp.jpg").setDirectoryName("temp").createFile()

            imageCapture?.takePicture(file, this,
                object : ImageCapture.OnImageSavedListener {

                    override fun onError(
                        error: ImageCapture.ImageCaptureError,
                        message: String, exc: Throwable?
                    ) {
                        val msg = ""
                    }

                    override fun onImageSaved(file: File) {
                        cameraViewModel.savePhoto(file)
                    }
                })
        }
    }

    private fun setCameraPublishPhotoListener() {
        cameraPublish.setOnClickListener {
            val intent = Intent(context, NewFeedActivity::class.java)
            val file = cameraViewModel.getSavedPhoto()
            intent.putExtra(Constants.CAMERA_SAVED_PHOTO_PATH_INTENT, file?.absolutePath)
            startActivity(intent)
        }
    }

    override fun execute(command: Runnable) {
        command.run()
    }

    val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)

    private fun hasNoPermissions(): Boolean{
        return ContextCompat
            .checkSelfPermission(requireContext(),
            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission(){
        ActivityCompat.requestPermissions(activity!!, permissions,0)
    }

}
