package com.farsky.izyactive.views.photoGallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.adapters.PhotoGalleryAdapter

class PhotoGalleryPlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PhotoGalleryViewModel
    private lateinit var photosAdapter: PhotoGalleryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pageViewModel = ViewModelProviders.of(this).get(PhotoGalleryViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }

        photosAdapter = PhotoGalleryAdapter(requireContext())

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_photo_gallery_placeholder, container, false)

        val rv = root.findViewById<RecyclerView>(R.id.photoGalleryRV)

        rv.setHasFixedSize(true)
        rv.setItemViewCacheSize(20)
        rv.itemAnimator = DefaultItemAnimator()
        rv.layoutManager = GridLayoutManager(context, 3)

        rv.adapter = photosAdapter

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pageViewModel.feedImages.observe(this, Observer {
            photosAdapter.setPhotos(it)
        })

    }

    companion object {

        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): PhotoGalleryPlaceholderFragment {
            return PhotoGalleryPlaceholderFragment()
                .apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }

}