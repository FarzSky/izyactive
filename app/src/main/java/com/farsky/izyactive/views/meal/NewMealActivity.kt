package com.farsky.izyactive.views.meal

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.farsky.izyactive.R

class NewMealActivity  : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_meal)
    }

}