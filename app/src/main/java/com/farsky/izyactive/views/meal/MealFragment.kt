package com.farsky.izyactive.views.meal

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.adapters.MealAdapter
import kotlinx.android.synthetic.main.fragment_meal.*

class MealFragment : Fragment() {

    private lateinit var viewModel: MealViewModel

    private lateinit var mealsAdapter: MealAdapter

    companion object {
        fun newInstance() = MealFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MealViewModel::class.java)

        mealsAdapter = MealAdapter(requireContext())

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_meal, container, false)

        val rv = view.findViewById<RecyclerView>(R.id.mealRV)

        rv.setHasFixedSize(true)
        rv.setItemViewCacheSize(20)
        rv.itemAnimator = DefaultItemAnimator()
        rv.layoutManager = LinearLayoutManager(context)

        rv.adapter = mealsAdapter

        return  view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        addMealFab.setOnClickListener {
            startActivity(Intent(context, NewMealActivity::class.java))
        }

        viewModel.meals.observe(this, Observer {
            mealsAdapter.setMeals(it)
        })

    }

}
