package com.farsky.izyactive.views.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.farsky.izyactive.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment : Fragment() {

    companion object {

        private const val ARG_SECTION_NUMBER = "main_section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): HomeFragment {
            return HomeFragment()
                .apply {
                    arguments = Bundle().apply {
                        putInt(ARG_SECTION_NUMBER, sectionNumber)
                    }
                }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)

        val bottomNav = root.findViewById<BottomNavigationView>(R.id.home_bot_nav_view)

        val navController = findNavController(root.findViewById(R.id.home_nav_host_fragment))


        bottomNav.setupWithNavController(navController)

        return root

    }



}
