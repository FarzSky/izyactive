package com.farsky.izyactive.views.workout

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.farsky.izyactive.data.models.dto.WorkoutDto
import com.farsky.izyactive.data.repositories.WorkoutRepository
import com.farsky.izyactive.utils.FileSaver
import com.farsky.izyactive.utils.Utils

class WorkoutViewModel(application: Application) : AndroidViewModel(application){

    var workoutRepository: WorkoutRepository = WorkoutRepository(application)

    var workouts: LiveData<List<WorkoutDto>>

    private val fileSaver = FileSaver(application)

    init {
        workouts = Transformations.map(
            workoutRepository.getWorkouts()
        )
        { input ->
            input.map { workout ->

                var imageLocationTemp = workout.imageLocation ?: ""
                if (imageLocationTemp != "") {
                    val dir = imageLocationTemp.substring(0, imageLocationTemp.lastIndexOf("/"))
                    val fileName = imageLocationTemp.substring(imageLocationTemp.lastIndexOf("/") + 1)
                    imageLocationTemp = fileSaver.setDirectoryName(dir).setFileName(fileName).getFilePath()
                }

                WorkoutDto(
                    imageLocation = imageLocationTemp,
                    name = workout.exercise,
                    time = if (workout.datetime == null) "" else Utils.dateTimeToEUString(workout.datetime!!),
                    duration = workout.duration,
                    calories = workout.calories
                )
            }
        }
    }

}
