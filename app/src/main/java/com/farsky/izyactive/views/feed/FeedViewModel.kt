package com.farsky.izyactive.views.feed

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.farsky.izyactive.data.models.dto.FeedRVDto
import com.farsky.izyactive.data.repositories.FeedRepository
import com.farsky.izyactive.utils.FileSaver

class FeedViewModel(application: Application) : AndroidViewModel(application) {

    private var feedRepository: FeedRepository = FeedRepository(application)

    var feeds: LiveData<List<FeedRVDto>>

    private val fileSaver = FileSaver(application)

    init {
        feeds = Transformations.map(
            feedRepository.getFeeds()
        )
        {
            input -> input.map { feed ->
                var imageLocationTemp = feed.imageLocation ?: ""
                if (imageLocationTemp != "") {
                    val dir = imageLocationTemp.substring(0, imageLocationTemp.lastIndexOf("/"))
                    val fileName = imageLocationTemp.substring(imageLocationTemp.lastIndexOf("/") + 1)
                    imageLocationTemp = fileSaver.setDirectoryName(dir).setFileName(fileName).getFilePath()
                }
                FeedRVDto(photo = imageLocationTemp,
                    description = feed.description,
                    feedType = feed.feedTypeId)
            }
        }

    }

}
