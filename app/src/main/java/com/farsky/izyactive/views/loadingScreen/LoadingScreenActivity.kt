package com.farsky.izyactive.views.loadingScreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.farsky.izyactive.R
import com.farsky.izyactive.utils.ApplicationPreferences
import com.farsky.izyactive.views.login.LoginActivity
import com.farsky.izyactive.views.main.MainActivity
import kotlinx.android.synthetic.main.activity_loading_screen.*

class LoadingScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading_screen)

        Glide.with(this).load(R.drawable.loading_screen).into(img_loading_screen)

        val loadingScreenThread = Thread {

            try {
                Thread.sleep(1000)
            } catch (e: Exception) {
                print(e.message)
            } finally {
                if (!isUserLoggedIn() ) {
                    startActivity(Intent(this, LoginActivity::class.java))
                } else {
                    startActivity(Intent(this, MainActivity::class.java))
                }

                finish()
            }

        }

        loadingScreenThread.start()

    }

    private fun isUserLoggedIn(): Boolean {
        val appPreferences: ApplicationPreferences = ApplicationPreferences.getInstance(this)
        return appPreferences.isUserLoggedIn
    }

}
