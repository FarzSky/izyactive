package com.farsky.izyactive.views.workout

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.adapters.WorkoutAdapter
import kotlinx.android.synthetic.main.fragment_workout.*

class WorkoutFragment : Fragment() {

    private lateinit var viewModel: WorkoutViewModel

    private lateinit var workoutAdapter: WorkoutAdapter

    companion object {
        fun newInstance() = WorkoutFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(WorkoutViewModel::class.java)

        workoutAdapter = WorkoutAdapter(requireContext())

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_workout, container, false)

        val rv = view.findViewById<RecyclerView>(R.id.workoutRV)

        rv.setHasFixedSize(true)
        rv.setItemViewCacheSize(20)
        rv.itemAnimator = DefaultItemAnimator()
        rv.layoutManager = LinearLayoutManager(context)

        rv.adapter = workoutAdapter

        return  view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        addWorkoutFab.setOnClickListener {
            startActivity(Intent(context, NewWorkoutActivity::class.java))
        }

        viewModel.workouts.observe(this, Observer {
            workoutAdapter.setWorkouts(it)
        })
    }

}
