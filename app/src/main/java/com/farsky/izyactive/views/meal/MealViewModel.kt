package com.farsky.izyactive.views.meal

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.farsky.izyactive.data.models.dto.MealDto
import com.farsky.izyactive.data.repositories.MealRepository
import com.farsky.izyactive.utils.FileSaver
import com.farsky.izyactive.utils.Utils

class MealViewModel(application: Application) : AndroidViewModel(application) {

    var mealRepository: MealRepository = MealRepository(application)

    var meals: LiveData<List<MealDto>>

    private val fileSaver = FileSaver(application)

    init {
        meals = Transformations.map(
            mealRepository.getMeals()
        )
        { input ->

            input.map { meal ->

                var imageLocationTemp = meal.imageLocation ?: ""
                if (imageLocationTemp != "") {
                    val dir = imageLocationTemp.substring(0, imageLocationTemp.lastIndexOf("/"))
                    val fileName = imageLocationTemp.substring(imageLocationTemp.lastIndexOf("/") + 1)
                    imageLocationTemp = fileSaver.setDirectoryName(dir).setFileName(fileName).getFilePath()
                }

                MealDto(
                    imageLocation = imageLocationTemp,
                    timeOfDay = meal.timeOfDay,
                    mealTime = if (meal.datetime == null) "" else Utils.dateTimeToEUString(meal.datetime!!),
                    mealCalories = meal.calories,
                    mealName = meal.name,
                    mealGroceries = meal.groceries
                )
            }
        }
    }

}
