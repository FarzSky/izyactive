package com.farsky.izyactive.views.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.farsky.izyactive.R
import com.farsky.izyactive.adapters.adapters.PhotoGallerySectionsAdapter
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    private lateinit var viewModel: ProfileViewModel
    private lateinit var photoGallerySectionsAdapter: PhotoGallerySectionsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)

        photoGallerySectionsAdapter =
            PhotoGallerySectionsAdapter(
                requireContext(),
                requireFragmentManager()
            )
        photoGalleryVP.adapter = photoGallerySectionsAdapter
        photoGalleryTabs.setupWithViewPager(photoGalleryVP)

    }

}
