package com.farsky.izyactive.data.models.dto

import com.farsky.izyactive.utils.Constants
import java.io.File

data class FeedDto(
    var photo: File?,
    var description: String = "",
    var feedType: Int = Constants.CODELIST_FEED_TYPE_PROFILE
)