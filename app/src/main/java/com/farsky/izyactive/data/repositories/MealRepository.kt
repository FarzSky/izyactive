package com.farsky.izyactive.data.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import com.farsky.izyactive.data.AppDatabase
import com.farsky.izyactive.data.dao.MealDao
import com.farsky.izyactive.data.models.entities.Meal
import java.util.*

class MealRepository(application: Application) {

    private var mealDao: MealDao

    private var meals: LiveData<List<Meal>>

    init {
        val db = AppDatabase.getDatabase(application)

        mealDao = db.mealDao()

        meals = mealDao.getMeals(Date())

    }

    fun getMeals(): LiveData<List<Meal>> {
        return meals
    }

}