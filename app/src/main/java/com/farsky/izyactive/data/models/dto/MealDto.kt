package com.farsky.izyactive.data.models.dto

import java.util.*

data class MealDto(
    val imageLocation: String? = "",
    val timeOfDay: String? = "",
    val mealTime: String? = "",
    val mealCalories: Int? = 0,
    val mealName: String? = "",
    val mealGroceries: List<String>? = emptyList()
)