package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Profile")
data class Profile(
    @PrimaryKey val email: String,
    val weight: Double = 0.0,
    val height: Double = 0.0,
    val bust: Double = 0.0,
    val waist: Double = 0.0,
    val hips: Double = 0.0,
    val calPerDay: Int = 0,
    val imageId: Long?
)