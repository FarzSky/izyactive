package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "MetadataType")
data class MetadataType(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val name: String
)