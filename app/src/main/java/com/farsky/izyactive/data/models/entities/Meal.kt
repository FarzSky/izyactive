package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Meal")
data class Meal(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ForeignKey(
        entity = Image::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("imageId"),
        onDelete = ForeignKey.SET_NULL
    )
    var imageId: Long? = null,
    var name: String? = "",
    var calories: Int? = 0,
    var datetime: Date? = Date(),
    @ForeignKey(
        entity = TimeOfDay::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("timeOfDayId"),
        onDelete = ForeignKey.SET_NULL
    )
    var timeOfDayId: Long? = null,
    @Ignore
    var groceries: List<String>? = emptyList(),
    @Ignore
    var timeOfDay: String? = "",
    @Ignore
    var imageLocation: String? = ""
)