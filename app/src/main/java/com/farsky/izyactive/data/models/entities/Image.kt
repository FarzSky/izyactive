package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Image")
data class Image(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    val url: String?,
    val path: String?,
    val name: String,
    val size: Int = 0,
    val created: Date
)