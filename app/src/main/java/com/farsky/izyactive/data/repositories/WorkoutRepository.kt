package com.farsky.izyactive.data.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import com.farsky.izyactive.data.AppDatabase
import com.farsky.izyactive.data.dao.WorkoutDao
import com.farsky.izyactive.data.models.entities.Workout
import java.util.*

class WorkoutRepository(application: Application) {

    private var workoutDao: WorkoutDao

    private var workouts: LiveData<List<Workout>>

    init {
        val db = AppDatabase.getDatabase(application)

        workoutDao = db.workoutDao()

        workouts = workoutDao.getWorkouts(Date())

    }

    fun getWorkouts(): LiveData<List<Workout>> {
        return workouts
    }

}