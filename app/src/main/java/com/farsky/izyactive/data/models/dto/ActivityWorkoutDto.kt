package com.farsky.izyactive.data.models.dto

data class ActivityWorkoutDto(
    val id: Long
)