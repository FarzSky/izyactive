package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Workout")
data class Workout(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ForeignKey(
        entity = Exercise::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("exerciseId"),
        onDelete = ForeignKey.SET_DEFAULT
    )
    var exerciseId: Long? = null,
    @ForeignKey(
        entity = Image::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("imageId"),
        onDelete = ForeignKey.SET_DEFAULT
    )
    var imageId: Long? = null,
    @ForeignKey(
        entity = Weather::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("weatherId"),
        onDelete = ForeignKey.SET_DEFAULT
    )
    var weatherId: Long? = null,
    var datetime: Date? = Date(),
    var duration: Int? = 0,
    var calories: Int? = 0,
    @Ignore
    var exercise: String? = "",
    @Ignore
    var imageLocation: String? = ""
)