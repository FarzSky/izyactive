package com.farsky.izyactive.data.enums

import androidx.camera.core.FlashMode
import com.farsky.izyactive.R

enum class CameraFlashlightEnum(val flashlightState: FlashMode, val flashDrawable: Int) {
    ON(FlashMode.ON, R.drawable.ic_flash_on_black_24dp), OFF(FlashMode.OFF, R.drawable.ic_flash_off_black_24dp)
}