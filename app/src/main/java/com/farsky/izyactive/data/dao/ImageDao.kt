package com.farsky.izyactive.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farsky.izyactive.data.models.entities.Image

@Dao
interface ImageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createImage(image: Image): Long

    @Query("SELECT * FROM Image JOIN Feed ON Image.id = Feed.imageId WHERE Feed.feedTypeId = :feedType ORDER BY datetime(Image.created) DESC")
    fun getImages(feedType: Long): LiveData<List<Image>>

}