package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Weather")
data class Weather(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val weather: String,
    val temperature: Double = 0.0
)
