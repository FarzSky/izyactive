package com.farsky.izyactive.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.farsky.izyactive.converters.DateConverter
import com.farsky.izyactive.data.dao.FeedDao
import com.farsky.izyactive.data.dao.ImageDao
import com.farsky.izyactive.data.dao.MealDao
import com.farsky.izyactive.data.dao.WorkoutDao
import com.farsky.izyactive.data.models.entities.*
import java.util.concurrent.Executors

@Database(entities = arrayOf(LoggedInUser::class,
    Account::class, Exercise::class, Feed::class, FeedMetadataMapping::class,
    Grocery::class, Image::class, Meal::class, MealGroceryMapping::class,
    Metadata::class, MetadataType::class, Profile::class, TimeOfDay::class,
    Weather::class, Workout::class, WorkoutLocator::class),
    version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun feedDao(): FeedDao
    abstract fun imageDao(): ImageDao
    abstract fun mealDao(): MealDao
    abstract fun workoutDao(): WorkoutDao

    companion object {

        const val DATABASE_NAME: String = "app_database"
        @Volatile private var INSTANCE: AppDatabase? = null
        private const val NUMBER_OF_THREADS = 4

        val databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS)

        fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java, DATABASE_NAME
                        )
                        .build()
                    }
                }
            }
            return INSTANCE!!
        }

    }

}