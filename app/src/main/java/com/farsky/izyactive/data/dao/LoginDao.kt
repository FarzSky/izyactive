package com.farsky.izyactive.data.dao

import com.farsky.izyactive.data.models.entities.LoggedInUser
import com.farsky.izyactive.data.results.Result
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDao {

    fun login(username: String): Result<LoggedInUser> {
        return try {
            // handle loggedInUser authentication
            val user = LoggedInUser(
                username
            )
            Result.Success(user)
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging in", e))
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun login(username: String, password: String): Result<LoggedInUser> {
        return try {
            // handle loggedInUser authentication
            val user = LoggedInUser(
                username
            )
            Result.Success(user)
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // revoke authentication
    }

}

