package com.farsky.izyactive.data.models.dto

data class ActivityMealDto(
    val id: Long
)