package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "Grocery")
data class Grocery(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val name: String,
    val calories: Int = 0,
    @ForeignKey(
        entity = Metadata::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("typeId"),
        onDelete = ForeignKey.SET_NULL
    )
    val typeId: Long?
)