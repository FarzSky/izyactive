package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "FeedMetadataMapping")
data class FeedMetadataMapping(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    @ForeignKey(
        entity = Feed::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("feedId"),
        onDelete = ForeignKey.CASCADE
    )
    val feedId: Long,
    @ForeignKey(
        entity = Metadata::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("metadataId"),
        onDelete = ForeignKey.CASCADE
    )
    val metadataId: Long
)