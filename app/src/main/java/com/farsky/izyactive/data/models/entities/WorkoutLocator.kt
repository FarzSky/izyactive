package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "WorkoutLocator")
data class WorkoutLocator(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    @ForeignKey(entity = Workout::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("workoutId"),
        onDelete = ForeignKey.CASCADE)
    val workoutId: Long,
    val latitude: Double = 0.0,
    val longitude: Double = 0.0
)