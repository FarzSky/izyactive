package com.farsky.izyactive.data.models.entities

import androidx.room.*
import java.util.*

@Entity(tableName = "Feed")
data class Feed(
    @PrimaryKey(autoGenerate = true) var id: Long?  = null,
    @ForeignKey(
        entity = Image::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("imageId"),
        onDelete = ForeignKey.CASCADE
    )
    var imageId: Long? = null,
    var description: String? = "",
    var created: Date? = Date(),
    var modified: Date? = Date(),
    @ForeignKey(
        entity = Metadata::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("feedTypeId"),
        onDelete = ForeignKey.SET_DEFAULT
    )
    var feedTypeId: Long? = null,
    @Ignore
    var imageLocation: String? = ""
)