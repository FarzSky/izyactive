package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Account")
data class Account(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    var created: Date = Date(),
    var private: Boolean = false
)