package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "MealGroceryMapping")
data class MealGroceryMapping(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    @ForeignKey(
        entity = Meal::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("mealId"),
        onDelete = ForeignKey.CASCADE
    )
    val mealId: Long,
    @ForeignKey(
        entity = Grocery::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("groceryId"),
        onDelete = ForeignKey.CASCADE
    )
    val groceryId: Long,
    val quantity: Int = 0,
    @ForeignKey(
        entity = Metadata::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("quantityTypeId"),
        onDelete = ForeignKey.SET_NULL
    )
    val quantityTypeId: Long
)