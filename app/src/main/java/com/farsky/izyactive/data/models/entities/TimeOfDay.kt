package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TimeOfDay")
data class TimeOfDay(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val name: String,
    val from: Int,
    val to: Int
)