package com.farsky.izyactive.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farsky.izyactive.data.models.entities.Feed

@Dao
interface FeedDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun createFeed(feed: Feed)

    @Query("SELECT Feed.id, Feed.imageId, Feed.description, Feed.created, Feed.modified, Feed.feedTypeId, Image.path AS imageLocation " +
            "FROM Feed JOIN Image ON Feed.imageId = Image.Id ORDER BY datetime(Feed.created)")
    fun getFeeds(): LiveData<List<Feed>>

}