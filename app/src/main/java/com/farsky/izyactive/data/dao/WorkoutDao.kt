package com.farsky.izyactive.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.farsky.izyactive.data.models.entities.Workout
import java.util.*

@Dao
interface WorkoutDao {

    @Query("SELECT Workout.id, Workout.exerciseId, Workout.weatherId, Workout.datetime, Workout.duration, Workout.calories, Exercise.name, Image.name " +
            "FROM Workout JOIN Image ON Workout.imageId = Image.id " +
            "JOIN Exercise ON Workout.exerciseId = Exercise.id " +
            "WHERE date(Workout.datetime) = date(:date) " +
            "ORDER BY datetime(Workout.datetime)")
    fun getWorkouts(date: Date): LiveData<List<Workout>>

}