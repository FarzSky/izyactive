package com.farsky.izyactive.data.models.dto

data class WorkoutDto(
    val imageLocation: String? = "",
    val name: String? = "",
    val time: String? = "",
    val duration: Int? = 0,
    val calories: Int? = 0
)