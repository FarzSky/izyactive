package com.farsky.izyactive.data.models.dto

data class ActivityBodyStatDto(
    val id: Long
)