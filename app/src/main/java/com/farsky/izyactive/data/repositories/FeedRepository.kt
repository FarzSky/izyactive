package com.farsky.izyactive.data.repositories

import android.app.Application
import androidx.lifecycle.LiveData
import com.farsky.izyactive.asyncs.WriteImageAsyncTask
import com.farsky.izyactive.data.AppDatabase
import com.farsky.izyactive.data.dao.FeedDao
import com.farsky.izyactive.data.dao.ImageDao
import com.farsky.izyactive.data.models.entities.Feed
import com.farsky.izyactive.data.models.entities.Image
import com.farsky.izyactive.utils.Constants
import com.farsky.izyactive.utils.Utils
import java.io.File
import java.util.*

class FeedRepository(public val application: Application) {

    private var feedDao: FeedDao
    private var imageDao: ImageDao

    private var feedTypeCache: Long = -1

    private lateinit var feedImages: LiveData<List<Image>>
    private lateinit var feeds: LiveData<List<Feed>>

    init {
        val db = AppDatabase.getDatabase(application)

        feedDao = db.feedDao()
        imageDao = db.imageDao()

        feeds = feedDao.getFeeds()

    }

    fun createFeed(feedType: Long, description: String, photo: File) {

        val now = Date()
        val nowFormated = Utils.dateTimeToString(now)

        val newFeed = Feed(
            id = null,
            created = now,
            modified = now,
            feedTypeId = feedType,
            description = description,
            imageId = 0
        )

        val newImage = Image(
            id = null,
            name = "$nowFormated.jpg",
            path = application.resources.getString(Constants.CODELIST_FEED_TYPE[feedType.toInt()] ?: Constants.CODELIST_FEED_TYPE[0]!!) +
                    "/" + "${nowFormated}.jpg",
            size = photo.length().toInt(),
            url = null,
            created = now
        )

        WriteImageAsyncTask().execute(WriteImageAsyncTask.ImageAsyncTaskParam(photo, newImage.path!!, application.applicationContext))

        AppDatabase.databaseWriteExecutor.execute {
            newImage.id = imageDao.createImage(newImage)
            newFeed.imageId = newImage.id!!
            feedDao.createFeed(newFeed)
        }

    }

    fun getImages(feedType: Long) : LiveData<List<Image>> {
        if (feedTypeCache != feedType) {
            feedTypeCache = feedType
            feedImages = imageDao.getImages(feedType)
        }
        return feedImages
    }

    fun getFeeds(): LiveData<List<Feed>> {
        return feeds
    }

}