package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Exercise")
data class Exercise(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val name: String,
    val description: String = "",
    val caloriesBurnPerMinute: Int = 0
)