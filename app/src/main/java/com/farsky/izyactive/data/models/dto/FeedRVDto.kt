package com.farsky.izyactive.data.models.dto

import com.farsky.izyactive.utils.Constants

data class FeedRVDto(
    var photo: String? = "",
    var description: String? = "",
    var feedType: Long? = Constants.CODELIST_FEED_TYPE_PROFILE.toLong()
)