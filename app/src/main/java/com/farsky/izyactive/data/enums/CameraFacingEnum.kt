package com.farsky.izyactive.data.enums

import androidx.camera.core.CameraX
import com.farsky.izyactive.R

enum class CameraFacingEnum(val cameraFacingState: CameraX.LensFacing, val facingDrawable: Int) {
    BACK(CameraX.LensFacing.BACK, R.drawable.ic_camera_rear_black_24dp), FRONT(CameraX.LensFacing.BACK, R.drawable.ic_camera_front_black_24dp)
}