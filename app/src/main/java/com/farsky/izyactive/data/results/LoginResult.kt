package com.farsky.izyactive.data.results

import com.farsky.izyactive.data.models.dto.LoggedInUserDto

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
    val success: LoggedInUserDto? = null,
    val error: Int? = null
)
