package com.farsky.izyactive.data.models.dto

data class PhotoGalleryDto(
    val location: String
)