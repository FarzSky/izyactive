package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
@Entity(tableName = "LoggedInUser")
data class LoggedInUser(
    @PrimaryKey val email: String
)
