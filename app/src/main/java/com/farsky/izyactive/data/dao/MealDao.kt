package com.farsky.izyactive.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farsky.izyactive.data.models.entities.Feed
import com.farsky.izyactive.data.models.entities.Meal
import java.util.*

@Dao
interface MealDao {

    @Query("SELECT m.id, m.imageId, m.name, m.calories, m.datetime, m.timeOfDayId, " +
            "(SELECT g.name FROM MealGroceryMapping AS mg JOIN Grocery AS g ON mg.groceryId = g.id WHERE mg.mealId = m.id) AS name, " +
            "meta.name, i.path " +
            "FROM Meal AS m JOIN Image AS i ON m.imageId = i.id " +
            "JOIN Metadata AS meta ON m.timeOfDayId = meta.id " +
            "WHERE date(m.datetime) = date(:date)" +
            "ORDER BY datetime(m.datetime)")
    fun getMeals(date: Date): LiveData<List<Meal>>

}