package com.farsky.izyactive.data.models.dto

/**
 * User details post authentication that is exposed to the UI
 */
data class LoggedInUserDto(
    val email: String
    //... other data fields that may be accessible to the UI
)
