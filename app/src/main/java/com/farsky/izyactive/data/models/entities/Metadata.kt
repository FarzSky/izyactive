package com.farsky.izyactive.data.models.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "Metadata")
data class Metadata(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    @ForeignKey(
        entity = MetadataType::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("metadataTypeId"),
        onDelete = ForeignKey.CASCADE
    )
    val metadataTypeId: Long,
    val name: String,
    val codelistId: Int?
)